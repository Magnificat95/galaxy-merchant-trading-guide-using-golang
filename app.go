package main

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"strconv"
	"strings"
	"github.com/StefanSchroeder/Golang-Roman"
)

type alienLanguageConvert struct {
	value string
}

var input_category = [][]string{}
var alien = map[string]*alienLanguageConvert{}

func main() {

	http.HandleFunc("/", index)
	http.ListenAndServe(":8000", nil)

}

func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "This is an example of Galaxy Merchant Trading Guide")
	fmt.Fprintln(w, "")
	fmt.Fprintln(w, "")
	fmt.Fprintln(w, "Input")
	fmt.Fprintln(w, "------------------")
	fmt.Fprintln(w, "")

	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		fmt.Print(err)
	}
	input_string := string(input)
	fmt.Fprintln(w, input_string)

	split_string := strings.Split(input_string, "\r")

	fmt.Fprintln(w, "")
	fmt.Fprintln(w, "Variable Values")
	fmt.Fprintln(w, "------------------")
	fmt.Fprintln(w, "")
	
	counter := 0
	initVal := []string{}
	credits := []string{}
	questions := []string{}
	for _, sentences := range split_string {
		if strings.Contains(sentences, "how"){
			//how - setQuestions
			questions = append(questions, split_string[counter])
		}else{
			if strings.Contains(sentences, "is"){
				if strings.Contains(sentences, "Credits"){
					//is credits !how - setCreditsValue
					credits = append(credits, split_string[counter])
				}else{ 
					//is !credits !how - setAlienConvert
					initVal = append(initVal, split_string[counter])
				}
			}
		}
		counter++
	}
	
	input_category = append(input_category, initVal)
	input_category = append(input_category, credits)
	input_category = append(input_category, questions)

	//process alienese to roman
	for _, item := range input_category[0] {
		value := strings.Split(item, " ")
		index := strings.TrimSpace(string(value[0]))
		alien[index] = &alienLanguageConvert{value: value[2]}
	}

	//process the value of metals
	for _, item := range input_category[1] {
		value := strings.Split(item, " ")
		word_1 := strings.TrimSpace(string(value[0]))
		word_2 := strings.TrimSpace(string(value[1]))
		roman_number := alien[word_1].value + alien[word_2].value
		number := roman.Arabic(roman_number)
		credits, _ := strconv.ParseFloat(value[4], 64)
		total := credits / float64(number)
		result := fmt.Sprintf("%f", total)
		alien[value[2]] = &alienLanguageConvert{value: result}
	}

	for key, items := range alien {
		fmt.Fprintln(w, key +" "+ items.value)
	}

	fmt.Fprintln(w, "")
	fmt.Fprintln(w, "Output")
	fmt.Fprintln(w, "------------------")
	fmt.Fprintln(w, "")

	//process questions
	for _, item := range input_category[2] {
		//filter out proper questions
		var result string
		var result_metal float64
		if strings.Contains(item, "is") && strings.Contains(item, "how")  {
				value := strings.Split(item, " ")
				for i := 0; i <= len(value)-1; i++ {
					if v, exists := alien[value[i]]; exists {
						if !isNumeric(v.value) {
							result = result + v.value
						}else {
							metal, _ := strconv.ParseFloat(v.value, 64)
							result_metal = float64(roman.Arabic(result)) * metal
						}
					}
				}
				fmt.Fprintf(w, item + " ")
				if result_metal > 0 {
					fmt.Fprintln(w, result_metal)
				}else {
					fmt.Fprintln(w, roman.Arabic(result))
				}
		} else {
			fmt.Fprintln(w, "")
			fmt.Fprintln(w, "I have no idea what you are talking about")
		}
	}
}

func isNumeric(s string) bool {
    _, err := strconv.ParseFloat(s, 64)
    return err == nil
}
